package kg.enesaitech.policlinic.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import kg.enesaitech.policlinic.dao.AppointmentHome;
import kg.enesaitech.policlinic.dao.WorkingHourHome;
import kg.enesaitech.policlinic.entity.TimePeriod;
import kg.enesaitech.policlinic.entity.WorkingHour;
import kg.enesaitech.policlinic.vo.SearchParameters;
import kg.enesaitech.policlinic.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimePeriodService {
	
	@Autowired
	private WorkingHourHome workingHourHome;
	
	@Autowired
	AppointmentHome appointmentHome;

	
	@Transactional
	public SearchResult<TimePeriod> getListByDoctorDate( Integer doctorId, Date date ) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.getSearchParameters().put("workingHourType.id", "1");
		SearchResult<TimePeriod> searchResult = new SearchResult<TimePeriod>();
		searchParameters.getSearchParameters().put("doctor.id", doctorId+"");
		Calendar c = Calendar.getInstance();
		c.setTime( date );
		int dayOfWeek = c.get( Calendar.DAY_OF_WEEK );
		dayOfWeek = (dayOfWeek+7-1)%7;
		searchParameters.getSearchParameters().put( "weekDay.id", dayOfWeek + "" );
		List<Integer> busyTimePeriodIds = appointmentHome.getTimePeriodIdsByDoctorDate(doctorId, date);
		
		SearchResult<WorkingHour> workingHours = workingHourHome.getList(searchParameters, WorkingHour.class);
		
		for(WorkingHour workingHour : workingHours.getResultList()){
			if(!busyTimePeriodIds.contains(workingHour.getTimePeriod().getId()))
				searchResult.getResultList().add(workingHour.getTimePeriod());
		}
		
		
		
		
		return searchResult;
	}

	
	
}