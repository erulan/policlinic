package kg.enesaitech.policlinic.service;

import java.util.Set;

import kg.enesaitech.policlinic.dao.PersonHome;
import kg.enesaitech.policlinic.entity.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {
	
	@Autowired
	private PersonHome staffHome;

	@Transactional
	public void create(Person person) {
		

		person.setPersonNumber(getPersonNumber());
		staffHome.persist(person);
	}
	
	public String getPersonNumber(){
		String personNo = "49999";
		
		Set<String> personNumbers = staffHome.personNumList();
		
		//Person number must be between 10000 and 50000;
		for(int number = 10000; number < 50000; number++){
			if(!personNumbers.contains(Integer.toString(number))){
				personNo = Integer.toString(number);
				break;
			}
		}
		
		return personNo;
	}
	
}