package kg.enesaitech.policlinic.service;

import kg.enesaitech.policlinic.entity.PatientPayment;
import kg.enesaitech.policlinic.entity.PatientPaymentService;
import kg.enesaitech.policlinic.entity.PoliclinicService;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.vo.PatientPaymentListVO;
import kg.enesaitech.policlinic.vo.PatientPaymentServiceVO;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PatientPaymentService1 {
	
	@Autowired
	private GenericHome<PatientPayment> patientPaymentHome;
	@Autowired
	private GenericHome<PatientPaymentService> patientPaymentServiceHome;
	@Autowired
	private GenericHome<PoliclinicService> serviceHome;
	@Autowired
	protected DozerBeanMapper mapper;	

	@Transactional
	public void createList(PatientPaymentListVO patientPaymentListVO) {
		PatientPayment patientPayment = mapper.map(patientPaymentListVO.getPatientPayment(), PatientPayment.class);
		Integer patientPaymentId = patientPaymentHome.persist(patientPayment);
		patientPayment.setId(patientPaymentId);
		Double total = 0.0;
		for(PatientPaymentServiceVO ppServiceVO : patientPaymentListVO.getPatientPaymentServices()){
			PatientPaymentService ppService = mapper.map(ppServiceVO, PatientPaymentService.class);
			ppService.setPatientPayment(patientPayment);
			patientPaymentServiceHome.persist(ppService);
			PoliclinicService service = serviceHome.findById(PoliclinicService.class, ppService.getService().getId());
			total += service.getPrice();
		}
		patientPayment.setTotalPrice(total);
		patientPaymentHome.merge(patientPayment);
	}
	
	
	
}