package kg.enesaitech.policlinic.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import kg.enesaitech.policlinic.dao.WorkingHourHome;
import kg.enesaitech.policlinic.entity.Doctor;
import kg.enesaitech.policlinic.entity.TimePeriod;
import kg.enesaitech.policlinic.entity.WeekDay;
import kg.enesaitech.policlinic.entity.WorkingHour;
import kg.enesaitech.policlinic.entity.WorkingHourType;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.vo.SearchParameters;
import kg.enesaitech.policlinic.vo.TimePeriodVO;
import kg.enesaitech.policlinic.vo.WorkingHourVO;
import kg.enesaitech.policlinic.vo.WorkingHourWeeklyVO;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WokringHourService {
	
	@Autowired
	private WorkingHourHome workingHourHome;
	
	@Autowired
	GenericHome<TimePeriod> timePeriodGenericHome;
	
	@Autowired
	GenericHome<WeekDay> weekDayGenericHome;

	@Autowired
	GenericHome<Doctor> doctorGenericHome;

	@Autowired
	GenericHome<WorkingHourType> workignHourTypeGenericHome;
		
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Transactional
	public List<WorkingHourWeeklyVO> getWorkingHourWeekly(int doctorId) {
		
		SearchParameters searchParameters = new SearchParameters();
		
		List<TimePeriod> timePeriods = timePeriodGenericHome.getList(searchParameters, TimePeriod.class).getResultList();
		List<WorkingHourWeeklyVO> hourWeeklyVOs = new ArrayList<WorkingHourWeeklyVO>();
		for(TimePeriod tp : timePeriods){
			if(!isTimePeriodDayTime(tp)) continue;
			WorkingHourWeeklyVO workingHourWeeklyVO = new WorkingHourWeeklyVO();
			workingHourWeeklyVO.setTimePeriod(mapper.map(tp, TimePeriodVO.class));
			hourWeeklyVOs.add(workingHourWeeklyVO);
		} 
		
		
		searchParameters.addParameter("doctor.id", String.valueOf(doctorId));
		List<WorkingHour> workingHours = workingHourHome.getList(searchParameters, WorkingHour.class).getResultList();
		
		for(WorkingHour wh : workingHours){
			if(!isTimePeriodDayTime(wh.getTimePeriod())) continue;
			WorkingHourVO workingHour = mapper.map(wh, WorkingHourVO.class);
			
			Integer index = 0;
			switch(workingHour.getWeekDay().getName()){
			case "Понедельник":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setMonday(workingHour);
				break;
			case "Вторник":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setTuesday(workingHour);
				break;
			case "Среда":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setWednesday(workingHour);
				break;
			case "Четверг":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setThirsday(workingHour);
				break;
			case "Пятница":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setFriday(workingHour);
				break;
			case "Суббота":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setSaturday(workingHour);
				break;
			case "Воскресенье":
				index = getIndexOfWH(workingHour, hourWeeklyVOs);
				hourWeeklyVOs.get(index).setSunday(workingHour);
				break;
			}
			
		}
		return hourWeeklyVOs;
	}

	private boolean isTimePeriodDayTime(TimePeriod timePeriod) {
		Integer startHour = Integer.valueOf(timePeriod.getStartHour().split(":")[0]);
		Integer endHour = Integer.valueOf(timePeriod.getEndHour().split(":")[0]);
		if(startHour >= 8 && endHour <= 20 && endHour > 0) return true;
		else return false;
	}

	private Integer getIndexOfWH(WorkingHourVO workingHour, List<WorkingHourWeeklyVO> hourWeeklyVOs) {
		Integer index = null;
		for(int i = 0; i <= hourWeeklyVOs.size()-1; i++){
			if(hourWeeklyVOs.get(i).getTimePeriod().getId().equals(workingHour.getTimePeriod().getId())) index = i;
		}
		return index;
	}

	@Transactional
	public void getWorkingHourWeekly(List<WorkingHourWeeklyVO> workingHourWeeklyVOs, Integer doctorId) {
		
		SearchParameters searchParameters = new SearchParameters();
		
		List<WeekDay> weekDays = weekDayGenericHome.getList(searchParameters, WeekDay.class).getResultList();
		List<WorkingHourType> workingHourTypes = workignHourTypeGenericHome.getList(searchParameters, WorkingHourType.class).getResultList();
		WorkingHourType nonworking = null;
		for(WorkingHourType wht : workingHourTypes){
			if(wht.getName().equals("нерабочее время")) nonworking = wht;
		}
		Doctor doctor = doctorGenericHome.findById(Doctor.class, doctorId);
		
		
		for(WorkingHourWeeklyVO wh : workingHourWeeklyVOs){	
			WorkingHour monday = new WorkingHour();
			WorkingHour tuesday = new WorkingHour();
			WorkingHour wednesday = new WorkingHour();
			WorkingHour thirsday = new WorkingHour();
			WorkingHour friday = new WorkingHour();
			WorkingHour saturday = new WorkingHour();
			WorkingHour sunday = new WorkingHour();
			
			monday.setDoctor(doctor);
			tuesday.setDoctor(doctor);
			wednesday.setDoctor(doctor);
			thirsday.setDoctor(doctor);
			friday.setDoctor(doctor);
			saturday.setDoctor(doctor);
			sunday.setDoctor(doctor);
			
			
			monday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			tuesday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			wednesday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			thirsday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			friday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			saturday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			sunday.setTimePeriod(mapper.map(wh.getTimePeriod(), TimePeriod.class));
			
			
			monday.setWeekDay(getWeekDays("Понедельник", weekDays));
			if(wh.getMonday() != null && wh.getMonday().getWorkingHourType() != null){
				monday.setWorkingHourType(mapper.map(wh.getMonday().getWorkingHourType(), WorkingHourType.class));
				monday.setId(wh.getMonday().getId());
			}else{
				monday.setWorkingHourType(nonworking);
			}

			
			tuesday.setWeekDay(getWeekDays("Вторник", weekDays));
			if(wh.getTuesday() != null && wh.getTuesday().getWorkingHourType() != null){
				tuesday.setWorkingHourType(mapper.map(wh.getTuesday().getWorkingHourType(), WorkingHourType.class));
				tuesday.setId(wh.getTuesday().getId());
			}else{
				tuesday.setWorkingHourType(nonworking);
			}

			wednesday.setWeekDay(getWeekDays("Среда", weekDays));
			if(wh.getWednesday() != null && wh.getWednesday().getWorkingHourType() != null){
				wednesday.setWorkingHourType(mapper.map(wh.getWednesday().getWorkingHourType(), WorkingHourType.class));
				wednesday.setId(wh.getWednesday().getId());
			}else{
				wednesday.setWorkingHourType(nonworking);
			}
			
			thirsday.setWeekDay(getWeekDays("Четверг", weekDays));
			if(wh.getThirsday() != null && wh.getThirsday().getWorkingHourType() != null){
				thirsday.setWorkingHourType(mapper.map(wh.getThirsday().getWorkingHourType(), WorkingHourType.class));
				thirsday.setId(wh.getThirsday().getId());
			}else{
				thirsday.setWorkingHourType(nonworking);
			}

			friday.setWeekDay(getWeekDays("Пятница", weekDays));
			if(wh.getFriday() != null && wh.getFriday().getWorkingHourType() != null){
				friday.setWorkingHourType(mapper.map(wh.getFriday().getWorkingHourType(), WorkingHourType.class));
				friday.setId(wh.getFriday().getId());
			}else{
				friday.setWorkingHourType(nonworking);
			}

			saturday.setWeekDay(getWeekDays("Суббота", weekDays));
			if(wh.getSaturday() != null && wh.getSaturday().getWorkingHourType() != null){
				saturday.setWorkingHourType(mapper.map(wh.getSaturday().getWorkingHourType(), WorkingHourType.class));
				saturday.setId(wh.getSaturday().getId());
			}else{
				saturday.setWorkingHourType(nonworking);
			}

			sunday.setWeekDay(getWeekDays("Воскресенье", weekDays));
			if(wh.getSunday() != null && wh.getSunday().getWorkingHourType() != null){
				sunday.setWorkingHourType(mapper.map(wh.getSunday().getWorkingHourType(), WorkingHourType.class));
				sunday.setId(wh.getSunday().getId());
			}else{
				sunday.setWorkingHourType(nonworking);
			}
			
			saveWorkingHour(monday);
			saveWorkingHour(tuesday);
			saveWorkingHour(wednesday);
			saveWorkingHour(thirsday);
			saveWorkingHour(friday);
			saveWorkingHour(saturday);
			saveWorkingHour(sunday);
			
		}
		
	}

	private void saveWorkingHour(WorkingHour workingHour) {
		if(workingHour.getId() == null){
			workingHourHome.persist(workingHour);
		}else{
			workingHourHome.merge(workingHour);
		}
	}

	private WeekDay getWeekDays(String string, List<WeekDay> weekDays) {
		for(WeekDay weekDay : weekDays){
			if(weekDay.getName().equals(string)) return weekDay;
		}
		return null;
	}

	
	
}