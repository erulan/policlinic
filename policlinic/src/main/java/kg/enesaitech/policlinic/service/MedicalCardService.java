package kg.enesaitech.policlinic.service;

import java.util.Set;

import kg.enesaitech.policlinic.dao.PersonHome;
import kg.enesaitech.policlinic.entity.MedicalCard;
import kg.enesaitech.policlinic.entity.Patient;
import kg.enesaitech.policlinic.generic.GenericHome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MedicalCardService {
	
	@Autowired
	private GenericHome<MedicalCard> medicalCardHome;
	@Autowired
	private PersonHome personHome;
	@Autowired
	private GenericHome<Patient> patientHome;

	@Transactional
	public void create(MedicalCard medicalCard) {
		medicalCard.setIsActive(true);
		medicalCard.getPatient().setPersonNumber(getPersonNumber());
		Integer patientId = patientHome.persist(medicalCard.getPatient());
		medicalCard.getPatient().setId(patientId);
		medicalCardHome.persist(medicalCard);
	}

	@Transactional
	public void update(MedicalCard medicalCard) {
		patientHome.merge(medicalCard.getPatient());
		medicalCardHome.merge(medicalCard);
	}

	@Transactional
	public void delete(int id) {
		MedicalCard medicalCard = medicalCardHome.findById(MedicalCard.class, id);
		Integer patientId = medicalCard.getPatient().getId();
		medicalCardHome.remove(medicalCard);
		patientHome.remove(patientHome.findById(Patient.class, patientId));
	}
	
	public String getPersonNumber(){
		String personNo = "49999";
		
		Set<String> personNumbers = personHome.personNumList();
		
		//Person number must be between 10000 and 50000;
		for(int number = 10000; number < 50000; number++){
			if(!personNumbers.contains(Integer.toString(number))){
				personNo = Integer.toString(number);
				break;
			}
		}
		
		return personNo;
	}
	
}