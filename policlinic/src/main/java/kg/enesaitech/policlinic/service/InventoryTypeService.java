package kg.enesaitech.policlinic.service;

import kg.enesaitech.policlinic.dao.InventoryHome;
import kg.enesaitech.policlinic.entity.InventoryType;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.vo.InventoryTypeVO;
import kg.enesaitech.policlinic.vo.SearchParameters;
import kg.enesaitech.policlinic.vo.SearchResult;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InventoryTypeService {
	@Autowired
	private GenericHome<InventoryType> inventoryTypeHome;
	@Autowired
	private InventoryHome inventoryHome;
	@Autowired
	protected DozerBeanMapper mapper;

	@Transactional
	public SearchResult<InventoryTypeVO> getList(SearchParameters searchParameters) {
		SearchResult<InventoryTypeVO> searchResultVO = new SearchResult<InventoryTypeVO>();
		SearchResult<InventoryType> searchResult = inventoryTypeHome.getList(searchParameters, InventoryType.class);
		for(InventoryType inventoryType : searchResult.getResultList()){
			Integer total = inventoryHome.getTotalInventories(inventoryType.getId());
			InventoryTypeVO inventoryTypeVO = mapper.map(inventoryType, InventoryTypeVO.class);
			inventoryTypeVO.setTotal(total);
			searchResultVO.getResultList().add(inventoryTypeVO);
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());
		return searchResultVO;
	}

}