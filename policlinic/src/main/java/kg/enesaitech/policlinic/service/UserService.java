package kg.enesaitech.policlinic.service;

import kg.enesaitech.policlinic.dao.UserHome;
import kg.enesaitech.policlinic.entity.Person;
import kg.enesaitech.policlinic.entity.User;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.vo.SearchParameters;
import kg.enesaitech.policlinic.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
	
	@Autowired
	private UserHome userHome;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private GenericHome<Person> personGenericHome;
	
	@Transactional
	public void createByStaffId(int id) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("person.id", String.valueOf(id));
		
		SearchResult<User> userList = userHome.getList(searchParameters, User.class);
		Person person;

		if(userList.getTotalRecords() > 0){
//			return Response.notModified("There is user already").build();
			//TODO
			return; 
		}
		else{
			person = personGenericHome.findById(Person.class, id);
		}

		if(person.getPersonNumber().isEmpty()){
			person.setPersonNumber(personService.getPersonNumber());
			personGenericHome.merge(person);
		}
		
		User user = new User(person, null, person.getPersonNumber(), userHome.getMD5(person.getPersonNumber() + "12345"));		
		
		userHome.persist(user);
	}

	public User getByUserName(String string) {
		return userHome.getByUserName(string);
	}
	
	@Transactional
	public User update(User detachedInstance) {
		detachedInstance.setPassword(userHome.getMD5(detachedInstance.getPassword()));
		return userHome.merge(detachedInstance);
	}
	
}