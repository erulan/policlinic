package kg.enesaitech.policlinic.providers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerProvider {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("barbulak");
	
	public static EntityManager getEntityManager(){
		return factory.createEntityManager();
	}
}
