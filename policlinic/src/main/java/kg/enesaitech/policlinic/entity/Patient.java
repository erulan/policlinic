package kg.enesaitech.policlinic.entity;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Patient generated by hbm2java
 */
@Entity
@Table(name = "patient")
@DiscriminatorValue("patient")
public class Patient extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
	private Set<MedicalCard> medicalCards = new HashSet<MedicalCard>(0);

	public Patient() {
	}


	public Patient(Person person, Set<MedicalCard> medicalCards) {
		this.medicalCards = medicalCards;
	}


	public Set<MedicalCard> getMedicalCards() {
		return this.medicalCards;
	}

	public void setMedicalCards(Set<MedicalCard> medicalCards) {
		this.medicalCards = medicalCards;
	}

}
