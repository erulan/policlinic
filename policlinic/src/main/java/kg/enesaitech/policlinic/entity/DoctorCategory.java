package kg.enesaitech.policlinic.entity;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kg.enesaitech.policlinic.generic.CommonEntity;

/**
 * MedicalCardType generated by hbm2java
 */
@Entity
@Table(name = "doctor_category")
public class DoctorCategory extends CommonEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "doctorCategory")
	private Set<Doctor> doctors = new HashSet<Doctor>(0);

	public DoctorCategory() {
	}

	public DoctorCategory(String name) {
		this.name = name;
	}

	public DoctorCategory(String name, Set<Doctor> doctors) {
		this.name = name;
		this.doctors = doctors;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}
}
