package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.WeekDay;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.WeekDayVO;


@Path("/week_day")
public class WeekDayWS extends GenericWS<WeekDay, WeekDayVO> {

	public WeekDayWS() {
		super(WeekDay.class, WeekDayVO.class);
	}
}