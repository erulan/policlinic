package kg.enesaitech.policlinic.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import kg.enesaitech.policlinic.entity.InventoryType;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.InventoryTypeService;
import kg.enesaitech.policlinic.vo.InventoryTypeVO;
import kg.enesaitech.policlinic.vo.SearchParameters;
import kg.enesaitech.policlinic.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/inventory_type")
public class InventoryTypeWS extends GenericWS<InventoryType, InventoryTypeVO> {

	public InventoryTypeWS() {
		super(InventoryType.class, InventoryTypeVO.class);
	}

	@Autowired
	InventoryTypeService inventoryTypeService;
	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<InventoryTypeVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<InventoryTypeVO> searchResultVO = inventoryTypeService.getList(searchParameters);
				

		return searchResultVO;

	}
}