package kg.enesaitech.policlinic.ws;



import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import kg.enesaitech.policlinic.entity.TimePeriod;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.TimePeriodService;
import kg.enesaitech.policlinic.vo.SearchDoctorDateVO;
import kg.enesaitech.policlinic.vo.SearchResult;
import kg.enesaitech.policlinic.vo.TimePeriodVO;

import org.springframework.beans.factory.annotation.Autowired;


@Path("/time_period")
public class TimePeriodWS extends GenericWS<TimePeriod, TimePeriodVO> {

	public TimePeriodWS() {
		super(TimePeriod.class, TimePeriodVO.class);
	}
	@Autowired
	TimePeriodService timePeriodService;

		
	@POST
	@Path("/get_list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<TimePeriodVO> getListByDoctorDate(SearchDoctorDateVO doctorDateVO) {
		if(doctorDateVO.getDoctorId()==null || doctorDateVO.getDate() == null){
			return new SearchResult<>();
		}
		SearchResult<TimePeriod> searchResult = timePeriodService.getListByDoctorDate(
				doctorDateVO.getDoctorId(), doctorDateVO.getDate());
		
		SearchResult<TimePeriodVO> searchResultVO = new SearchResult<TimePeriodVO>();

		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		
		return searchResultVO;
	}
	
}