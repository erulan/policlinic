package kg.enesaitech.policlinic.ws;

import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.InventoryStatus;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.InventoryStatusVO;

@Path("/inventory_status")
public class InventoryStatusWS extends GenericWS<InventoryStatus, InventoryStatusVO> {

	public InventoryStatusWS() {
		super(InventoryStatus.class, InventoryStatusVO.class);
	}
}