package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Doctor;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.DoctorVO;


@Path("/doctor")
public class DoctorWS extends GenericWS<Doctor, DoctorVO> {

	public DoctorWS() {
		super(Doctor.class, DoctorVO.class);
	}
}