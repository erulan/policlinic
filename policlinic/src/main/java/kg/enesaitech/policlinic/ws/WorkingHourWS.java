package kg.enesaitech.policlinic.ws;



import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.policlinic.entity.WorkingHour;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.WokringHourService;
import kg.enesaitech.policlinic.vo.WorkingHourVO;
import kg.enesaitech.policlinic.vo.WorkingHourWeeklyVO;

import org.springframework.beans.factory.annotation.Autowired;


@Path("/working_hour")
public class WorkingHourWS extends GenericWS<WorkingHour, WorkingHourVO> {

	public WorkingHourWS() {
		super(WorkingHour.class, WorkingHourVO.class);
	}
	
	@Autowired
	WokringHourService workingHourService;
	
	@GET
	@Path("/working_hour_weekly/{doctor_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<WorkingHourWeeklyVO> getWorkingHourWeekly(@PathParam("doctor_id") int doctorId) {
		
		List<WorkingHourWeeklyVO> workingHourWeeklyList = 
				workingHourService.getWorkingHourWeekly(doctorId);
		
		return workingHourWeeklyList;
	}
	
	@POST
	@Path("/working_hour_weekly_save/{doctor_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response workingHourWeeklySave(List<WorkingHourWeeklyVO> workingHourWeeklyVOs, @PathParam("doctor_id") int doctorId) {
		
		workingHourService.getWorkingHourWeekly(workingHourWeeklyVOs, doctorId);
		return Response.ok().build();
	}
}