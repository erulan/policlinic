package kg.enesaitech.policlinic.ws;

import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.InventoryCategory;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.InventoryCategoryVO;

@Path("/inventory_category")
public class InventoryCategoryWS extends GenericWS<InventoryCategory, InventoryCategoryVO> {

	public InventoryCategoryWS() {
		super(InventoryCategory.class, InventoryCategoryVO.class);
	}
}
