package kg.enesaitech.policlinic.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.policlinic.entity.Inventory;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.InventoryService;
import kg.enesaitech.policlinic.vo.InventoryVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/inventory")
public class InventoryWS extends GenericWS<Inventory, InventoryVO> {

	public InventoryWS() {
		super(Inventory.class, InventoryVO.class);
	}
	@Autowired
	private InventoryService inventoryService;	

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(InventoryVO vo) {
		Inventory e = mapper.map(vo, Inventory.class);
		
		inventoryService.create(e);
		
		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(InventoryVO vo, @PathParam("id") int id) {
				
		Inventory e2 = genericService.get(Inventory.class, id);
		
		if(e2 == null || e2.getId() == null || (vo.getId() != null && e2.getId() != vo.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Inventory e = mapper.map(vo, Inventory.class);
		inventoryService.update(e);
		return Response.ok().build();
	}
}