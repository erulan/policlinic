package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Appointment;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.AppointmentVO;


@Path("/appointment")
public class AppointmentWS extends GenericWS<Appointment, AppointmentVO> {

	public AppointmentWS() {
		super(Appointment.class, AppointmentVO.class);
	}
}