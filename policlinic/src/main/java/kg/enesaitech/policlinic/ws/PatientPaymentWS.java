package kg.enesaitech.policlinic.ws;



import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.policlinic.entity.PatientPayment;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.PatientPaymentService1;
import kg.enesaitech.policlinic.vo.PatientPaymentListVO;
import kg.enesaitech.policlinic.vo.PatientPaymentVO;

import org.springframework.beans.factory.annotation.Autowired;


@Path("/patient_payment")
public class PatientPaymentWS extends GenericWS<PatientPayment, PatientPaymentVO> {

	public PatientPaymentWS() {
		super(PatientPayment.class, PatientPaymentVO.class);
	}
	@Autowired
	PatientPaymentService1 patientPaymentService;
	
	@POST
	@Path("/create_list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createList(PatientPaymentListVO patientPaymentListVO) {
		patientPaymentService.createList(patientPaymentListVO);		
		return Response.ok().build();
	}
}