package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.PoliclinicService;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.ServiceVO;


@Path("/service")
public class ServiceWS extends GenericWS<PoliclinicService, ServiceVO> {

	public ServiceWS() {
		super(PoliclinicService.class, ServiceVO.class);
	}
}