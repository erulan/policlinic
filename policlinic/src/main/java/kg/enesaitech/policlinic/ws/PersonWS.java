package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Person;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.PersonVO;


@Path("/person")
public class PersonWS extends GenericWS<Person, PersonVO> {

	public PersonWS() {
		super(Person.class, PersonVO.class);
	}
}