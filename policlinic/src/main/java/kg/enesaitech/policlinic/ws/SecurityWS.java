package kg.enesaitech.policlinic.ws;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import kg.enesaitech.policlinic.dao.RoleHome;
import kg.enesaitech.policlinic.dao.UserHome;
import kg.enesaitech.policlinic.entity.Person;
import kg.enesaitech.policlinic.entity.Staff;
import kg.enesaitech.policlinic.entity.User;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.generic.GenericService;
import kg.enesaitech.policlinic.service.UserService;
import kg.enesaitech.policlinic.userException.BusinessException;
import kg.enesaitech.policlinic.vo.PersonVO;
import kg.enesaitech.policlinic.vo.StaffVO;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/security")
public class SecurityWS {
//	RoleService roleService = RoleService.getInstance();
	@Autowired
	RoleHome roleHome;

	@Autowired
	UserService userService;
	
	@Autowired
	UserHome usersHome;
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	GenericService<Person, GenericHome<Person>> genericPersonService;
	
	Logger log = Logger.getLogger(SecurityWS.class);

	@GET
	@Path("/redirect")
	public Response authenticate() {
		Response response = Response.ok().build();
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			
			String username = currentUser.getPrincipal().toString();
			Set<String> rolesAsString = roleHome.getNameSetByUserName(username);
			log.warn("************************************************************");
			log.warn("Roles for User in SecurityWS: ["+currentUser.getPrincipal().toString()+"] : " + rolesAsString.toString());
			log.warn("************************************************************");

			if (rolesAsString.contains("ceo")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/ceo"))
						.build();
			} else if (rolesAsString.contains("reception")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/reception"))
						.build();
			} else if (rolesAsString.contains("cashier")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/cashier"))
						.build();
			} else if (rolesAsString.contains("hr_manager")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/hr_manager"))
						.build();
			} else if (rolesAsString.contains("accountant")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/accountant"))
						.build();
			} else if (rolesAsString.contains("chief_nurse")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/chief_nurse"))
						.build();
			} else if (rolesAsString.contains("chief_doctor")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/chief_doctor"))
								.build();
			} else if (rolesAsString.contains("laboratory_assistant")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/laboratory_assistant"))
								.build();
			} else if (rolesAsString.contains("warehouse_manager")) {
				response = Response
						.seeOther(
								URI.create("/policlinic/apps/warehouse_manager"))
								.build();
			}else {
				response = Response.seeOther(URI.create("/unauthorized.html"))
						.build();
			}
		} else {
//			throw new UnauthorizedException();
			response = Response.seeOther(
					URI.create("/policlinic/login.html")).build();
		}
		return response;
	}

	@GET
	@Path("/current_user")
	@Produces("application/json")
	public PersonVO getCurrentUser() {
		Subject currentUser = SecurityUtils.getSubject();

		User user = usersHome.getByUserName(currentUser.getPrincipal().toString());
		
		if(user.getPerson() != null){
			Person person = genericPersonService.get(Person.class, user.getPerson().getId());
			PersonVO personVO = mapper.map(person, PersonVO.class);
			return personVO;
		}else{
			return new PersonVO();
		}
	}
	
	@GET
	@Path("/roles_of_current_user")
	@Produces("application/json")
	public Set<String> getRolesOfCurrentUser() {
		return roleHome.getNameSetByUserName(SecurityUtils.getSubject().getPrincipal().toString());
	}
	
	@GET
	@Path("/get_username")
	@Produces("application/json")
	public Response getUsername() {
		Subject currentUser = SecurityUtils.getSubject();
		return Response.ok().entity("{\"username\": \"" + currentUser.getPrincipal().toString() + "\"}").build();
	}

//	this method changes password for current user and it needs 
//	userame, current_p, new_p, new_p_repeat;
	@POST
	@Path("/change_password")
	public Response changePassword(Map<String, String> passwordChange){
		Response response = Response.status(Status.OK).build();
		String username = passwordChange.get("username");
		String currentPass = passwordChange.get("current_p");
		String newPass = passwordChange.get("new_p");
		String newPassRepeat = passwordChange.get("new_p_repeat");
		if(!newPass.equals(newPassRepeat)){throw new BusinessException("Неправильный повтор нового пароля");}
		User user = usersHome.getByUserName(username);
		Subject currentUser = SecurityUtils.getSubject();
		
		if(!currentUser.isAuthenticated() || !user.getUsername().equals(currentUser.getPrincipal())){
			throw new BusinessException("Access denied");
		}

		if(usersHome.getMD5(currentPass).equals(user.getPassword())){
			user.setPassword(newPass);
			userService.update(user);
		}else{
			throw new BusinessException("Current password doesn't match ");
		}
			
		return response;
	}
}
