package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.MedicalCardProfile;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.MedicalCardProfileVO;


@Path("/medical_card_profile")
public class MedicalCardProfileWS extends GenericWS<MedicalCardProfile, MedicalCardProfileVO> {

	public MedicalCardProfileWS() {
		super(MedicalCardProfile.class, MedicalCardProfileVO.class);
	}
}