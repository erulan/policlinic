package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.User;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.UserVO;


@Path("/user")
public class UserWS extends GenericWS<User, UserVO> {

	public UserWS() {
		super(User.class, UserVO.class);
	}
}