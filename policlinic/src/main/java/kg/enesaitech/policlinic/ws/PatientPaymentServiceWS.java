package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.PatientPaymentService;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.PatientPaymentServiceVO;


@Path("/patient_payment_service")
public class PatientPaymentServiceWS extends GenericWS<PatientPaymentService, PatientPaymentServiceVO> {

	public PatientPaymentServiceWS() {
		super(PatientPaymentService.class, PatientPaymentServiceVO.class);
	}
}