package kg.enesaitech.policlinic.ws;



import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.policlinic.entity.MedicalCard;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.service.MedicalCardService;
import kg.enesaitech.policlinic.userException.BusinessException;
import kg.enesaitech.policlinic.vo.MedicalCardVO;

import org.springframework.beans.factory.annotation.Autowired;


@Path("/medical_card")
public class MedicalCardWS extends GenericWS<MedicalCard, MedicalCardVO> {

	public MedicalCardWS() {
		super(MedicalCard.class, MedicalCardVO.class);
	}

	@Autowired
	private MedicalCardService medicalCardService;
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(MedicalCardVO medicalCardVO) {
		MedicalCard medicalCard = mapper.map(medicalCardVO, MedicalCard.class);		
		medicalCardService.create(medicalCard);
		
		return Response.ok().build();
	}
	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(MedicalCardVO medicalCardVO, @PathParam("id") int id) {
				
		MedicalCard e2 = genericService.get(MedicalCard.class, id);
		
		if(e2 == null || e2.getId() == null || medicalCardVO.getPatient().getId()==null 
				|| !medicalCardVO.getPatient().getId().equals(e2.getPatient().getId()) ||
				(medicalCardVO.getId() != null && !medicalCardVO.getId().equals(medicalCardVO.getId()))){
			log.error("Id sent in object doesn't match path id!!!");
			throw new BusinessException("Id sent in object doesn't match path id!!!");
		}
		MedicalCard e = mapper.map(medicalCardVO, MedicalCard.class);
		e.getPatient().setPersonNumber(e2.getPatient().getPersonNumber());
		medicalCardService.update(e);
		return Response.ok().build();
	}
	

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
				
		medicalCardService.delete(id);
		
		return Response.ok().build();
	}
}