package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Role;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.RoleVO;


@Path("/role")
public class RoleWS extends GenericWS<Role, RoleVO> {

	public RoleWS() {
		super(Role.class, RoleVO.class);
	}
}