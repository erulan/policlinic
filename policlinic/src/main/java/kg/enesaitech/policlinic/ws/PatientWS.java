package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Patient;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.PatientVO;


@Path("/patient")
public class PatientWS extends GenericWS<Patient, PatientVO> {

	public PatientWS() {
		super(Patient.class, PatientVO.class);
	}
}