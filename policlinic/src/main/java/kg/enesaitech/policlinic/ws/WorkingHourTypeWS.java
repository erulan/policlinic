package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.WorkingHourType;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.WorkingHourTypeVO;


@Path("/working_hour_type")
public class WorkingHourTypeWS extends GenericWS<WorkingHourType, WorkingHourTypeVO> {

	public WorkingHourTypeWS() {
		super(WorkingHourType.class, WorkingHourTypeVO.class);
	}
}