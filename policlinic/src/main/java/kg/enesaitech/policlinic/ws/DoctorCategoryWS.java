package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.DoctorCategory;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.DoctorCategoryVO;


@Path("/doctor_category")
public class DoctorCategoryWS extends GenericWS<DoctorCategory, DoctorCategoryVO> {

	public DoctorCategoryWS() {
		super(DoctorCategory.class, DoctorCategoryVO.class);
	}
}