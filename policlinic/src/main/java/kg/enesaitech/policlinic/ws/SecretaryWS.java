package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Secretary;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.SecretaryVO;


@Path("/secretary")
public class SecretaryWS extends GenericWS<Secretary, SecretaryVO> {

	public SecretaryWS() {
		super(Secretary.class, SecretaryVO.class);
	}
}