package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.Staff;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.StaffVO;


@Path("/staff")
public class StaffWS extends GenericWS<Staff, StaffVO> {

	public StaffWS() {
		super(Staff.class, StaffVO.class);
	}
}