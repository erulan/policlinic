package kg.enesaitech.policlinic.ws;



import javax.ws.rs.Path;

import kg.enesaitech.policlinic.entity.MedicalCardType;
import kg.enesaitech.policlinic.generic.GenericWS;
import kg.enesaitech.policlinic.vo.MedicalCardTypeVO;


@Path("/medical_card_type")
public class MedicalCardTypeWS extends GenericWS<MedicalCardType, MedicalCardTypeVO> {

	public MedicalCardTypeWS() {
		super(MedicalCardType.class, MedicalCardTypeVO.class);
	}
}