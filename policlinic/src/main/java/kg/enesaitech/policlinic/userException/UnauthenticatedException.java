package kg.enesaitech.policlinic.userException;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;
 
/** Throw this exception to return a 401 Unauthorized response.  The WWW-Authenticate header is
 * set appropriately and a short message is included in the response entity. */
public class UnauthenticatedException extends WebApplicationException
{
    private static final long serialVersionUID = 1L;
 
    public UnauthenticatedException(String message)
    {
        super(Response.seeOther( UriBuilder.fromUri("/barbulak/unauthorized.html").build() ).entity("{\"message:\" \"" + message +"\"}").build());
    }
}