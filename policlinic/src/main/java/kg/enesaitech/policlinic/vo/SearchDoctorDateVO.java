package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.policlinic.generic.CommonVO;

public class SearchDoctorDateVO extends CommonVO {
	
	
	private Integer doctorId;
	private Date date;

	public SearchDoctorDateVO() {
	}

	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
