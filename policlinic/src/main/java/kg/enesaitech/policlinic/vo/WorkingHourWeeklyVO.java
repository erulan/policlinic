package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class WorkingHourWeeklyVO {
	
	private TimePeriodVO timePeriod;

	private WorkingHourVO monday;
	
	private WorkingHourVO tuesday;
	
	private WorkingHourVO wednesday;
	
	private WorkingHourVO thirsday;
	
	private WorkingHourVO friday;
	
	private WorkingHourVO saturday;
	
	private WorkingHourVO sunday;

	public WorkingHourVO getMonday() {
		return monday;
	}

	public void setMonday(WorkingHourVO monday) {
		this.monday = monday;
	}

	public WorkingHourVO getTuesday() {
		return tuesday;
	}

	public void setTuesday(WorkingHourVO tuesday) {
		this.tuesday = tuesday;
	}

	public WorkingHourVO getWednesday() {
		return wednesday;
	}

	public void setWednesday(WorkingHourVO wednesday) {
		this.wednesday = wednesday;
	}

	public WorkingHourVO getThirsday() {
		return thirsday;
	}

	public void setThirsday(WorkingHourVO thirsday) {
		this.thirsday = thirsday;
	}

	public WorkingHourVO getFriday() {
		return friday;
	}

	public void setFriday(WorkingHourVO friday) {
		this.friday = friday;
	}

	public WorkingHourVO getSaturday() {
		return saturday;
	}

	public void setSaturday(WorkingHourVO saturday) {
		this.saturday = saturday;
	}

	public WorkingHourVO getSunday() {
		return sunday;
	}

	public void setSunday(WorkingHourVO sunday) {
		this.sunday = sunday;
	}

	public TimePeriodVO getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(TimePeriodVO timePeriod) {
		this.timePeriod = timePeriod;
	}
}
