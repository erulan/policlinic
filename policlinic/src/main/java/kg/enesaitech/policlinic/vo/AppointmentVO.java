package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.policlinic.generic.CommonVO;

public class AppointmentVO extends CommonVO {
	
	
	private MedicalCardVO medicalCard;
	private DoctorVO doctor;
	private Date date;
	private TimePeriodVO timePeriod;

	public AppointmentVO() {
	}

	public AppointmentVO(MedicalCardVO medicalCard, DoctorVO doctor) {
		this.medicalCard = medicalCard;
		this.doctor = doctor;
	}

	public AppointmentVO(MedicalCardVO medicalCard, DoctorVO doctor, Date date) {
		this.medicalCard = medicalCard;
		this.doctor = doctor;
		this.date = date;
	}
	
	public MedicalCardVO getMedicalCard() {
		return this.medicalCard;
	}

	public void setMedicalCard(MedicalCardVO medicalCard) {
		this.medicalCard = medicalCard;
	}
	
	public DoctorVO getDoctor() {
		return this.doctor;
	}

	public void setDoctor(DoctorVO doctor) {
		this.doctor = doctor;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public TimePeriodVO getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(TimePeriodVO timePeriod) {
		this.timePeriod = timePeriod;
	}

}
