package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;

/**
 * MedicalCardType generated by hbm2java
 */

public class DoctorCategoryVO extends CommonVO{

	private String name;

	private Set<DoctorVO> doctors = new HashSet<DoctorVO>(0);

	public DoctorCategoryVO() {
	}

	public DoctorCategoryVO(String name) {
		this.name = name;
	}

	public DoctorCategoryVO(String name, Set<DoctorVO> doctors) {
		this.name = name;
		this.doctors = doctors;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<DoctorVO> getDoctors() {
		return doctors;
	}

	public void setDoctors(Set<DoctorVO> doctors) {
		this.doctors = doctors;
	}

}
