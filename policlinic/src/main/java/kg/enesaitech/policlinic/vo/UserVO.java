package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class UserVO extends CommonVO {

	
	private PersonVO person;
	private RoleVO role;
	private String username;
	private String password;

	public UserVO() {
	}

	public UserVO(PersonVO person, RoleVO role, String username, String password) {
		this.person = person;
		this.role = role;
		this.username = username;
		this.password = password;
	}


	public PersonVO getPerson() {
		return this.person;
	}

	public void setPerson(PersonVO person) {
		this.person = person;
	}

	public RoleVO getRole() {
		return this.role;
	}

	public void setRole(RoleVO role) {
		this.role = role;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
