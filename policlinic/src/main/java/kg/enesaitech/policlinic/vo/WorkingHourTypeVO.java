package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class WorkingHourTypeVO extends CommonVO {

	private String name;

	public WorkingHourTypeVO() {
	}

	public WorkingHourTypeVO(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
