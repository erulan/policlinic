package kg.enesaitech.policlinic.vo;

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;

public class MedicalCardTypeVO extends CommonVO {

	private String name;
	private Set<MedicalCardVO> medicalCards = new HashSet<MedicalCardVO>(0);

	public MedicalCardTypeVO() {
	}

	public MedicalCardTypeVO(String name) {
		this.name = name;
	}

	public MedicalCardTypeVO(String name, Set<MedicalCardVO> medicalCards) {
		this.name = name;
		this.medicalCards = medicalCards;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<MedicalCardVO> getMedicalCards() {
		return this.medicalCards;
	}

	public void setMedicalCards(Set<MedicalCardVO> medicalCards) {
		this.medicalCards = medicalCards;
	}

}
