package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class PatientPaymentServiceVO extends CommonVO {

	private ServiceVO service;
	private PatientPaymentVO patientPayment;

	public PatientPaymentServiceVO() {
	}

	public ServiceVO getService() {
		return service;
	}

	public void setService(ServiceVO service) {
		this.service = service;
	}

	public PatientPaymentVO getPatientPayment() {
		return patientPayment;
	}

	public void setPatientPayment(PatientPaymentVO patientPayment) {
		this.patientPayment = patientPayment;
	}

}
