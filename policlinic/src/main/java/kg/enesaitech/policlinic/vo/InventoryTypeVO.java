package kg.enesaitech.policlinic.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;


public class InventoryTypeVO extends CommonVO{

	private InventoryCategoryVO inventoryCategory;
	private String name;
	private Set<InventoryVO> inventories = new HashSet<InventoryVO>(0);
	private Integer total;

	public InventoryTypeVO() {
	}

	public InventoryTypeVO(InventoryCategoryVO inventoryCategory, String name) {
		this.inventoryCategory = inventoryCategory;
		this.name = name;
	}

	public InventoryTypeVO(InventoryCategoryVO inventoryCategory, String name,
			Set<InventoryVO> inventories) {
		this.inventoryCategory = inventoryCategory;
		this.name = name;
		this.inventories = inventories;
	}
	
	public InventoryCategoryVO getInventoryCategory() {
		return this.inventoryCategory;
	}

	public void setInventoryCategory(InventoryCategoryVO inventoryCategory) {
		this.inventoryCategory = inventoryCategory;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<InventoryVO> getInventories() {
		return this.inventories;
	}

	public void setInventories(Set<InventoryVO> inventories) {
		this.inventories = inventories;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
