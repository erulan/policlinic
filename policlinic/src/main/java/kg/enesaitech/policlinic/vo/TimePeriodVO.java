package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class TimePeriodVO extends CommonVO {

	/**
	 * author Azamatshekin
	 */
	
	private String startHour;
	private String endHour;


	public TimePeriodVO() {
	}


	public String getStartHour() {
		return startHour;
	}


	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}


	public String getEndHour() {
		return endHour;
	}


	public void setEndHour(String endHour) {
		this.endHour = endHour;
	}


}