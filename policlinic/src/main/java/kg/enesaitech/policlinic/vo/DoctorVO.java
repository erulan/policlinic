package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.entity.DoctorCategory;

public class DoctorVO extends PersonVO{

	private String speciality;
	private String workExperieince;
	private DoctorCategory doctorCategory;
	private Date doctorCategoryDate;
	private Date lastSpecialityDate;
	private Date employmentStartDate;
	
	private Set<WorkingHourVO> workingHours = new HashSet<WorkingHourVO>(0);
	private Set<AppointmentVO> appointments = new HashSet<AppointmentVO>(0);

	public DoctorVO() {
	}

	public DoctorVO(Set<WorkingHourVO> workingHours,
			Set<AppointmentVO> appointments) {
		this.appointments = appointments;
	}

	public Set<WorkingHourVO> getWorkingHours() {
		return this.workingHours;
	}

	public void setWorkingHours(Set<WorkingHourVO> workingHours) {
		this.workingHours = workingHours;
	}


	public Set<AppointmentVO> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(Set<AppointmentVO> appointments) {
		this.appointments = appointments;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getWorkExperieince() {
		return workExperieince;
	}

	public void setWorkExperieince(String workExperieince) {
		this.workExperieince = workExperieince;
	}

	public DoctorCategory getDoctorCategory() {
		return doctorCategory;
	}

	public void setDoctorCategory(DoctorCategory doctorCategory) {
		this.doctorCategory = doctorCategory;
	}

	public Date getDoctorCategoryDate() {
		return doctorCategoryDate;
	}

	public void setDoctorCategoryDate(Date doctorCategoryDate) {
		this.doctorCategoryDate = doctorCategoryDate;
	}

	public Date getLastSpecialityDate() {
		return lastSpecialityDate;
	}

	public void setLastSpecialityDate(Date lastSpecialityDate) {
		this.lastSpecialityDate = lastSpecialityDate;
	}

	public Date getEmploymentStartDate() {
		return employmentStartDate;
	}

	public void setEmploymentStartDate(Date employmentStartDate) {
		this.employmentStartDate = employmentStartDate;
	}

}
