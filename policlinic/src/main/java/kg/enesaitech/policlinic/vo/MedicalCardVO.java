package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;

public class MedicalCardVO extends CommonVO {

	private PatientVO patient;
	private MedicalCardTypeVO medicalCardType;
	private Boolean isActive;
	private Date activatedDate;
	private String medCardNum;
	private Date passivatedDate;
	private MedicalCardProfileVO medicalCardProfile;
	private Set<AppointmentVO> appointments = new HashSet<AppointmentVO>(0);

	public MedicalCardVO() {
	}

	public MedicalCardVO(PatientVO patient, MedicalCardTypeVO medicalCardType) {
		this.patient = patient;
		this.medicalCardType = medicalCardType;
	}

	public MedicalCardVO(PatientVO patient, MedicalCardTypeVO medicalCardType,
			Boolean isActive, Date activatedDate, Date passivatedDate,
			Set<MedicalCardProfileVO> medicalCardProfiles, Set<AppointmentVO> appointments) {
		this.patient = patient;
		this.medicalCardType = medicalCardType;
		this.isActive = isActive;
		this.activatedDate = activatedDate;
		this.passivatedDate = passivatedDate;
		this.appointments = appointments;
	}

	public PatientVO getPatient() {
		return this.patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public MedicalCardTypeVO getMedicalCardType() {
		return this.medicalCardType;
	}

	public void setMedicalCardType(MedicalCardTypeVO medicalCardType) {
		this.medicalCardType = medicalCardType;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getActivatedDate() {
		return this.activatedDate;
	}

	public void setActivatedDate(Date activatedDate) {
		this.activatedDate = activatedDate;
	}

	public Date getPassivatedDate() {
		return this.passivatedDate;
	}

	public void setPassivatedDate(Date passivatedDate) {
		this.passivatedDate = passivatedDate;
	}

	public Set<AppointmentVO> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(Set<AppointmentVO> appointments) {
		this.appointments = appointments;
	}

	public String getMedCardNum() {
		return medCardNum;
	}

	public void setMedCardNum(String medCardNum) {
		this.medCardNum = medCardNum;
	}

	public MedicalCardProfileVO getMedicalCardProfile() {
		return medicalCardProfile;
	}

	public void setMedicalCardProfile(MedicalCardProfileVO medicalCardProfile) {
		this.medicalCardProfile = medicalCardProfile;
	}

}
