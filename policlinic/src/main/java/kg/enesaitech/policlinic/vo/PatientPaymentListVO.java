package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import kg.enesaitech.policlinic.generic.CommonVO;

public class PatientPaymentListVO extends CommonVO {
	
	
	private PatientPaymentVO patientPayment;
	private List<PatientPaymentServiceVO> patientPaymentServices;

	public PatientPaymentListVO() {
	}

	public PatientPaymentVO getPatientPayment() {
		return patientPayment;
	}

	public void setPatientPayment(PatientPaymentVO patientPayment) {
		this.patientPayment = patientPayment;
	}

	public List<PatientPaymentServiceVO> getPatientPaymentServices() {
		return patientPaymentServices;
	}

	public void setPatientPaymentServices(List<PatientPaymentServiceVO> patientPaymentServices) {
		this.patientPaymentServices = patientPaymentServices;
	}
}
