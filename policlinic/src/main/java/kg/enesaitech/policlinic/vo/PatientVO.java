package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

public class PatientVO extends PersonVO {

	private PersonVO person;
	private Set<MedicalCardVO> medicalCards = new HashSet<MedicalCardVO>(0);

	public PatientVO() {
	}

	public PatientVO(PersonVO person) {
		this.person = person;
	}

	public PatientVO(PersonVO person, Set<MedicalCardVO> medicalCards) {
		this.person = person;
		this.medicalCards = medicalCards;
	}
	
	public PersonVO getPerson() {
		return this.person;
	}

	public void setPerson(PersonVO person) {
		this.person = person;
	}

	public Set<MedicalCardVO> getMedicalCards() {
		return this.medicalCards;
	}

	public void setMedicalCards(Set<MedicalCardVO> medicalCards) {
		this.medicalCards = medicalCards;
	}

}
