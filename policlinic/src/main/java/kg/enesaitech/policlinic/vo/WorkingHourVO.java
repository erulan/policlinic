package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.policlinic.generic.CommonVO;

public class WorkingHourVO extends CommonVO {

	private WeekDayVO weekDay;
	private DoctorVO doctor;
	private String room;
    private TimePeriodVO timePeriod;
    private WorkingHourTypeVO workingHourType;

	public WorkingHourVO() {
	}

	public WorkingHourVO(WeekDayVO weekDay, DoctorVO doctor, Date startHour) {
		this.weekDay = weekDay;
		this.doctor = doctor;
	}


	public WeekDayVO getWeekDay() {
		return this.weekDay;
	}

	public void setWeekDay(WeekDayVO weekDay) {
		this.weekDay = weekDay;
	}

	public DoctorVO getDoctor() {
		return this.doctor;
	}

	public void setDoctor(DoctorVO doctor) {
		this.doctor = doctor;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public TimePeriodVO getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(TimePeriodVO timePeriod) {
		this.timePeriod = timePeriod;
	}

	public WorkingHourTypeVO getWorkingHourType() {
		return workingHourType;
	}

	public void setWorkingHourType(WorkingHourTypeVO workingHourType) {
		this.workingHourType = workingHourType;
	}


}
