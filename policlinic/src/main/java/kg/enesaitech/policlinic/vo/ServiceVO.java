package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.policlinic.generic.CommonVO;

public class ServiceVO extends CommonVO {

	/**
	 * author Azamatshekin
	 */
	
	private String name;
	private Double price;


	public ServiceVO() {
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


}