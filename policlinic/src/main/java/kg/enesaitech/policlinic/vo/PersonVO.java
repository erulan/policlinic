package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;


public class PersonVO extends CommonVO {

	private String name;
	private String surname;
	private String middleName;
	private Date birthDate;
	private String address;
	private String mobilePhone;
	private String homeNumber;
	private String personType;
	private Set<UserVO> users = new HashSet<UserVO>();

	public PersonVO() {
	}

	public PersonVO(String name, Date birthDate) {
		this.name = name;
		this.birthDate = birthDate;
	}

	public PersonVO(String name, String surname, String middleName,
			Date birthDate, String address, String mobilePhone,
			String homeNumber, String personType, Set<UserVO> users) {
		this.name = name;
		this.surname = surname;
		this.middleName = middleName;
		this.birthDate = birthDate;
		this.address = address;
		this.mobilePhone = mobilePhone;
		this.homeNumber = homeNumber;
		this.personType = personType;
		this.users = users;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getHomeNumber() {
		return this.homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String getPersonType() {
		return this.personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public Set<UserVO> getUsers() {
		return this.users;
	}

	public void setUsers(Set<UserVO> users) {
		this.users = users;
	}

}
