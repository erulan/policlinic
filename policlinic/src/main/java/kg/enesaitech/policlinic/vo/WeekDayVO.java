package kg.enesaitech.policlinic.vo;

// default package
// Generated Jun 17, 2015 11:05:08 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.policlinic.generic.CommonVO;

public class WeekDayVO extends CommonVO {

	private String name;
	private Set<WorkingHourVO> workingHours = new HashSet<WorkingHourVO>(0);

	public WeekDayVO() {
	}

	public WeekDayVO(String name) {
		this.name = name;
	}

	public WeekDayVO(String name, Set<WorkingHourVO> workingHours) {
		this.name = name;
		this.workingHours = workingHours;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<WorkingHourVO> getWorkingHours() {
		return this.workingHours;
	}

	public void setWorkingHours(Set<WorkingHourVO> workingHours) {
		this.workingHours = workingHours;
	}

}
