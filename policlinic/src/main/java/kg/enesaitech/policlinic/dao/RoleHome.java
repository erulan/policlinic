package kg.enesaitech.policlinic.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.policlinic.entity.Role;
import kg.enesaitech.policlinic.generic.GenericHome;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */

@Repository
public class RoleHome extends GenericHome<Role>{

	private static final Log log = LogFactory.getLog(RoleHome.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	//This method is created to be used by shiro realm
	public Set<String> getNameSetByUserName(String username) {
		Set<String> roleNames = null;
		try {
			Query query = entityManager.createQuery(" SELECT r.name FROM Role r LEFT JOIN r.users u WHERE u.username = :username");
			query.setParameter("username", username);
			roleNames = new HashSet(query.getResultList());
		} catch (RuntimeException re) {
			log.error("getListByUsername Error", re);
		}
		return roleNames;
	}

}
