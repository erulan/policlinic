package kg.enesaitech.policlinic.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.policlinic.entity.Appointment;
import kg.enesaitech.policlinic.generic.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class AppointmentHome extends GenericHome<Appointment>  {

	@PersistenceContext
	private EntityManager entityManager;
	

	public List<Integer> getTimePeriodIdsByDoctorDate( Integer doctorId, Date date ) {
		try {
			Query query = entityManager.createQuery(" SELECT b.timePeriod.id FROM Appointment b WHERE "
					+ " b.doctor.id = :doctorId and b.date = :date ");
			query.setParameter("doctorId", doctorId);
			query.setParameter("date", date);

			@SuppressWarnings("unchecked")
			List<Integer> timePeriodIds = query.getResultList();
			return timePeriodIds;
		
		} catch (RuntimeException re) {
			log.error("list timePeriodIds from appointment error", re);
			throw re;
		}
	}
}
