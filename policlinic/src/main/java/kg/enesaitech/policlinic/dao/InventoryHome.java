package kg.enesaitech.policlinic.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.policlinic.entity.Inventory;
import kg.enesaitech.policlinic.generic.GenericHome;
import kg.enesaitech.policlinic.others.InventoryStatus;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class InventoryHome extends GenericHome<Inventory> {

	@PersistenceContext
	private EntityManager entityManager;

	
	public Integer getTotalInventories(int inventoryTypeId) {
		try{
			Query query = entityManager.createQuery("SELECT COUNT(*) FROM Inventory b where b.inventoryType.id = :inventoryTypeId ");
			query.setParameter("inventoryTypeId", inventoryTypeId);
			Integer total = Integer.parseInt(query.getSingleResult().toString());
			return total;
		} catch (RuntimeException re) {
			log.error("count total inventory by inventoryType error", re);
			throw re;
		}
	}
	//This method is created to be used by shiro realm
		public Inventory getByInventoryNO(String inventoryNo) {
			Inventory user = null;
			try {
				Query query = entityManager.createQuery("FROM Inventory b where b.inventoryNo = :inventoryNo");
				query.setParameter("inventoryNo", inventoryNo);
				user = (Inventory) query.getSingleResult();
			} catch (RuntimeException re) {
				return null;
			}
			
			return user;
		}
		
		//this method is to write off(списание)
		public List<Inventory> getListForWriteOff(){
			try{
				Query query = entityManager.createQuery("FROM Inventory b where b.endDate < CURRENT_DATE() and b.inventoryStatus.name = '"+InventoryStatus.OWNED_BY_COMPANY.getName()+"' ");
				List<Inventory> inventories = query.getResultList();
				return inventories;
			} catch (RuntimeException re) {
				log.error("count total inventory by inventoryType error", re);
				throw re;
			}
		}
		
}
