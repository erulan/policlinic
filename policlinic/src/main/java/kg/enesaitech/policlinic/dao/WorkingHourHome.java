package kg.enesaitech.policlinic.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import kg.enesaitech.policlinic.entity.WorkingHour;
import kg.enesaitech.policlinic.generic.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class WorkingHourHome extends GenericHome<WorkingHour>  {

	@PersistenceContext
	private EntityManager entityManager;
	

}
