'use strict';

/* Controllers */
  // signin controller


app.controller('PatientListCtrl', ['$scope', 'Utils', function($scope, Utils) {
	$scope.tableParams = Utils.standardNgTableInit("medical_card");
}]);
  
app.controller('PatientAddCtrl', ['$scope', '$state', 'restService', function($scope, $state, restService) {

	$scope.medicalCard = {};
    $scope.medicalCard.patient = {};
    $scope.medicalCard.medicalCardType = {};
    $scope.medicalCard.medicalCardProfile = {};
    $scope.medicalCard.activatedDate = new Date();
    
    restService.all("medical_card_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_types = data.originalElement.resultList;
	});
    
    restService.all("medical_card_profile").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_profiles = data.originalElement.resultList;
	});
	
    $scope.create = function(){
    	restService.one("medical_card").customPOST($scope.medicalCard, "create").then(function(data){
    		alert("Успешно создано!");
    		$state.go("reception.patient.list");
    	});
    }
   
}]);
    
app.controller('PatientEditCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {
    $scope.medicalCard = {};
    $scope.medicalCard.patient = {};
    $scope.medicalCard.medicalCardType = {};
    $scope.medicalCard.medicalCardProfile = {};
    $scope.medicalCardId = $stateParams.id;
    
    restService.all("medical_card").customGET( $scope.medicalCardId).then(function(data){
		$scope.medicalCard= data.originalElement;
	});
    restService.all("medical_card_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_types = data.originalElement.resultList;
	});
    restService.all("medical_card_profile").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_profiles = data.originalElement.resultList;
	});
	
  
    $scope.update = function(){
    	restService.one("medical_card").customPOST($scope.medicalCard, "update/" + $scope.medicalCardId).then(function(data){
    		alert("Успешно обновлен!");
    		$state.go("reception.patient.list");
    	});
    }
  }]);

