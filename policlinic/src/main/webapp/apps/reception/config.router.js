'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/reception/patient/list');
          $stateProvider
              .state('reception', {
                  abstract: true,
                  url: '/reception',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //patient
              .state('reception.patient', {
                  abstract: true,
                  url: '/patient',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/reception/patient/controller.js']);
                      }]
                    }
              })
              .state('reception.patient.list', {
                  url: '/list',
                  templateUrl: '../../apps/reception/patient/list.html',
              })
              .state('reception.patient.add', {
                  url: '/add',
                  templateUrl: '../../apps/reception/patient/add.html',
              })
              .state('reception.patient.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/reception/patient/edit.html',

              })
              
              
              //appointment
              .state('reception.appointment', {
                  abstract: true,
                  url: '/appointment',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/reception/appointment/controller.js']);
                      }]
                    }
              })
              .state('reception.appointment.list', {
                  url: '/list',
                  templateUrl: '../../apps/reception/appointment/list.html',
              })
              .state('reception.appointment.add', {
                  url: '/add',
                  templateUrl: '../../apps/reception/appointment/add.html',
              })
              .state('reception.appointment.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/reception/appointment/edit.html',
              })
              .state('reception.appointment.view', {
                  url: '/view/:id',
                  templateUrl: '../../apps/reception/appointment/view.html',
              })
      }
    ]
  );
