'use strict';

/* Controllers */
  // signin controller

app.controller('AppointmentListCtrl', ['$scope', 'Utils',  function($scope, Utils) {
	
	$scope.tableParams = Utils.standardNgTableInit("appointment");
	
}]);
  
app.controller('AppointmentAddCtrl', ['$scope', '$state', 'restService', function($scope, $state, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.appointment = {};
    $scope.appointment.medicalCard = {};
    $scope.appointment.timePeriod = {};
    $scope.appointment.doctor = {};
    $scope.doctor_date = {};
	
	
    
    restService.all("appointment").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.appointments = data.originalElement.resultList;
	});

    restService.all("medical_card").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_cards = data.originalElement.resultList;
	});
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	restService.one("appointment").customPOST($scope.appointment, "create").then(function(data){
    		alert("Успешно создано!");
    		$state.go("reception.appointment.list");
    	});
    }
    $scope.getTimePeroids = function(){
    	$scope.doctor_date.date = $scope.appointment.date;
    	$scope.doctor_date.doctorId = $scope.appointment.doctor.id;

        restService.all("time_period").customPOST($scope.doctor_date, "get_list").then(function(data){
    		$scope.time_periods = data.originalElement.resultList;
    	});
    }
    
	 
  }])
  
  /*
  $scope.placeByFromPlaceId = {};
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;

		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};
		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	restService.one("security").customGET('current_user').then(function(data){
        		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});

	    			$scope.getProductsList($defer, params, "product_place");
        		});
    		});
        },
    });
*/
    
app.controller('AppointmentEditCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {

    $scope.appointmentId = $stateParams.id;
 
    restService.all("appointment").customGET( $scope.appointmentId ).then(function(data){
		$scope.appointment = data.originalElement;
	});
    restService.all("medical_card").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_cards = data.originalElement.resultList;
	});
    restService.all("time_period").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.timePeriods = data.originalElement.resultList;
	});
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    
	 
    $scope.update = function(){
    	restService.one("appointment").customPOST($scope.appointment, "update/" + $scope.appointmentId).then(function(data){
    		alert("Успешно обновлен!");
    		$state.go("reception.appointment.list");
    	});
    }
  }]);

app.controller('AppointmentViewCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {

    $scope.appointment = {};
    
    $scope.appointmentId = $stateParams.id;
    
    restService.all("appointment").customGET( $scope.appointmentId ).then(function(data){
		$scope.appointment = data.originalElement;
	});
    restService.all("medical_card").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medicalCard = data.originalElement;
	});
    restService.all("medical_card_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_types = data.originalElement.resultList;
	});
    restService.all("medical_card_profile").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.medical_card_profiles = data.originalElement.resultList;
	});
 /*   $scope.update = function(){
    	restService.one("appointment").customPOST($scope.appointment, "update/" + $scope.appointmentId).then(function(data){
    		alert("Успешно обновлен!");
    		$state.go("reception.appointment.list");
    	});
    }*/
  }]);
