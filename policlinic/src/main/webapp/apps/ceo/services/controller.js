'use strict';

/* Controllers */
  // signin controller


app.controller('ServiceListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', '$state' , 
                                   function($scope, restService, ngTableParams, Utils, $modal, $state) {
	

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "service");
        },
    });
	
	
	$scope.deleteService = function(id){
		$scope.deleting("service", id);
	};
	$scope.deleting = function(deletepath, id){
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'sm'
		    });

		    modalInstance.result.then(function (selectedItem) {
		    	restService.one(deletepath).customGET("delete/" + id).then(function(data){
		    		alert("Successfully deleted!");
		    		alert("Извините, перезагрузите страницу!");
		    	});
		    }, function () {
		      return;
		    });
	}
  }]);
  
app.controller('ServiceAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {

    $scope.service = {};
    
    restService.all("service").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.services = data.originalElement.resultList;
	});
	
    $scope.create = function(){
    	restService.one("service").customPOST($scope.service, "create").then(function(data){
    		alert("Успешно создано!");
    		$location.path("/services/list");
    	});
    }
	 
  }])
    
app.controller('ServiceEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
  
	$scope.service = {};
	$scope.serviceId = $stateParams.id;
    
    restService.all("service").customGET( $scope.serviceId).then(function(data){
		$scope.services = data.originalElement.resultList;
	});
	
	 
    $scope.update = function(){
    	restService.one("service").customPOST($scope.service, "update/" + $scope.serviceId).then(function(data){
    		alert("Успешно обновлено!");
    		$location.path("/services/list");
    	});
    }
  }]);

