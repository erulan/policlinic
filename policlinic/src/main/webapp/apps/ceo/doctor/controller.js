'use strict';

/* Controllers */
  // DOCTOR controllers


app.controller('DoctorListCtrl', ['$scope', 'Utils',  function($scope, Utils) {
		
	$scope.tableParams = Utils.standardNgTableInit("doctor");
	
}]);
  
app.controller('DoctorAddCtrl', ['$scope', '$state', 'restService', function($scope, $state, restService) {

	$scope.doctor = {};
	$scope.doctor.doctorCategory = {};
 
    
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    
    restService.all("doctor_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctorCategories = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	restService.one("doctor").customPOST($scope.doctor, "create").then(function(data){
    		alert("Успешно создано!");
    		$state.go('ceo.doctor.list')
    	});
    };

}])
    
app.controller('DoctorEditCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {

	$scope.doctorId = $stateParams.id;
	
	restService.all("doctor").customGET( $scope.doctorId).then(function(data){
		$scope.doctor= data.originalElement;
	});
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    restService.all("doctor_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctorCategories = data.originalElement.resultList;
	});
    
    $scope.update = function(){
    	restService.one("doctor").customPOST($scope.doctor, "update/" + $scope.doctorId).then(function(data){
    		alert("Успешно обновлено!");
    		$state.go("ceo.doctor.list");
    	});
    }
  }]);

