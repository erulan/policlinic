'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/ceo/services/list');
          $stateProvider
              .state('ceo', {
                  abstract: true,
                  url: '/ceo',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //services
              .state('ceo.services', {
                  abstract: true,
                  url: '/services',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/ceo/services/controller.js']);
                      }]
                    }
              })
              .state('ceo.services.list', {
                  url: '/list',
                  templateUrl: '../../apps/ceo/services/list.html',
              })
              .state('ceo.services.add', {
                  url: '/add',
                  templateUrl: '../../apps/ceo/services/add.html',
              })
              .state('ceo.services.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/ceo/services/edit.html',

              })
              
              
              //working_hour
              .state('ceo.working_hour', {
                  abstract: true,
                  url: '/working_hour',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/ceo/working_hour/controller.js']);
                      }]
                    }
              })
              .state('ceo.working_hour.list', {
                  url: '/list',
                  templateUrl: '../../apps/ceo/working_hour/list.html',
              })
              .state('ceo.working_hour.add', {
                  url: '/add',
                  templateUrl: '../../apps/ceo/working_hour/add.html',
              })
              .state('ceo.working_hour.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/ceo/working_hour/edit.html',

              })
              .state('ceo.working_hour.general', {
                  url: '/general',
                  templateUrl: '../../apps/ceo/working_hour/general.html',
              })
              
              //doctor
              .state('ceo.doctor', {
                  abstract: true,
                  url: '/doctor',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/ceo/doctor/controller.js']);
                      }]
                    }
              })
              .state('ceo.doctor.list', {
                  url: '/list',
                  templateUrl: '../../apps/ceo/doctor/list.html',
              })
              .state('ceo.doctor.add', {
                  url: '/add',
                  templateUrl: '../../apps/ceo/doctor/add.html',
              })
              .state('ceo.doctor.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/ceo/doctor/edit.html',

              })
      }
    ]
  );
