'use strict';

/* Controllers */
  // signin controller


app.controller('WorkingHourListCtrl', ['$scope' , 'Utils',  function($scope, Utils) {
	$scope.tableParams = Utils.standardNgTableInit("working_hour");
  }]);

app.controller('GeneralCtrl', ['$scope' , '$state', 'restService', function($scope, $state, restService) {
	
	restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
	
	$scope.$watch('doctorId', function(newVal, oldVal){
		if(newVal == null){return;}
		restService.all("working_hour").customGET('working_hour_weekly/' + $scope.doctorId).then(function(data){
			$scope.workingHourWeeklies = data.originalElement;
			angular.forEach($scope.workingHourWeeklies, function(value, key){
				if(value.monday == null){
					$scope.workingHourWeeklies[key].monday = {};
				}
				if(value.tuesday == null){
					$scope.workingHourWeeklies[key].tuesday = {};
				}
				if(value.wednesday == null){
					$scope.workingHourWeeklies[key].wednesday = {};
				}
				if(value.thirsday == null){
					$scope.workingHourWeeklies[key].thirsday = {};
				}
				if(value.friday == null){
					$scope.workingHourWeeklies[key].friday = {};
				}
				if(value.saturday == null){
					$scope.workingHourWeeklies[key].saturday = {};
				}
				if(value.sunday == null){
					$scope.workingHourWeeklies[key].sunday = {};
				}
			});
		});
	});
		
	restService.all("working_hour_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.workingHourTypes = data.originalElement.resultList;
	});
	
	$scope.save = function(){
		restService.all("working_hour").customPOST($scope.workingHourWeeklies, ['working_hour_weekly_save/' + $scope.doctorId]).then(function(data){
			alert("Успешно обновлено!");
    		$state.go("ceo.working_hour.list");
		});
	}
  }]);
  
app.controller('WorkingHourAddCtrl', ['$scope', '$state', 'restService', function($scope, $state, restService) {

	$scope.workingHour = {};
    $scope.workingHour.doctor = {};
    $scope.workingHour.weekDay= {};
    $scope.workingHour.timePeriod= {};
    $scope.workingHour.workingHourType= {};

    
    restService.all("week_day").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.week_days = data.originalElement.resultList;
	});
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    restService.all("time_period").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.time_periods = data.originalElement.resultList;
	});
    restService.all("working_hour_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.working_hour_types = data.originalElement.resultList;
	});
    $scope.create = function(){
    	restService.one("working_hour").customPOST($scope.workingHour, "create").then(function(data){
    		alert("Успешно создано!");
    		$state.go("ceo.working_hour.list");
    	});
    }
}])
    
app.controller('WorkingHourEditCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {

	$scope.workingHour = {};
    $scope.workingHour.doctor = {};
    $scope.workingHour.weekDay= {};
    $scope.workingHour.timePeriod= {};
    $scope.workingHour.workingHourType= {};
	$scope.workingHourId = $stateParams.id;

    
	restService.all("working_hour").customGET( $scope.workingHourId).then(function(data){
		$scope.workingHour= data.originalElement;
	});
    restService.all("week_day").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.week_days = data.originalElement.resultList;
	});
    restService.all("doctor").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.doctors = data.originalElement.resultList;
	});
    restService.all("time_period").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.time_periods = data.originalElement.resultList;
	});
    restService.all("working_hour_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.working_hour_types = data.originalElement.resultList;
	});
    $scope.update = function(){
    	restService.one("working_hour").customPOST($scope.workingHour, "update/" + $scope.workingHourId).then(function(data){
    		alert("Успешно обновлено!");
    		$state.go("ceo.working_hour.list");
    	});
    }
  }]);

app.filter('split', function() {
    return function(input) {
    	return input.split(' ')[0];
    }
});
