'use strict';

/* Controllers */
  // DOCTOR controllers


app.controller('SecretaryListCtrl', ['$scope', 'Utils',  function($scope, Utils) {
		
	$scope.tableParams = Utils.standardNgTableInit("secretary");
	
}]);
  
app.controller('SecretaryAddCtrl', ['$scope', '$state', 'restService', function($scope, $state, restService) {

	$scope.secretary = {};
 
    
    restService.all("secretary").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.secretaries = data.originalElement.resultList;
	});
    
    
    $scope.create = function(){
    	restService.one("secretary").customPOST($scope.secretary, "create").then(function(data){
    		alert("Успешно создано!");
    		$state.go('hr_manager.secretary.list')
    	});
    };

}])
    
app.controller('SecretaryEditCtrl', ['$scope', '$state', '$stateParams', 'restService', function($scope, $state, $stateParams, restService) {

	$scope.secretaryId = $stateParams.id;
	
	restService.all("secretary").customGET( $scope.secretaryId).then(function(data){
		$scope.secretary= data.originalElement;
	});
    restService.all("secretary").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.secretaries = data.originalElement.resultList;
	});
    
    $scope.update = function(){
    	restService.one("secretary").customPOST($scope.secretary, "update/" + $scope.secretaryId).then(function(data){
    		alert("Успешно обновлено!");
    		$state.go("hr_manager.secretary.list");
    	});
    }
  }]);

