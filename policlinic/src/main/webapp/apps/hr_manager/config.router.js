'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/hr_manager/doctor/list');
          $stateProvider
              .state('hr_manager', {
                  abstract: true,
                  url: '/hr_manager',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //patient
              .state('hr_manager.doctor', {
                  abstract: true,
                  url: '/doctor',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/hr_manager/doctor/controller.js']);
                      }]
                    }
              })
              .state('hr_manager.doctor.list', {
                  url: '/list',
                  templateUrl: '../../apps/hr_manager/doctor/list.html',
              })
              .state('hr_manager.doctor.add', {
                  url: '/add',
                  templateUrl: '../../apps/hr_manager/doctor/add.html',
              })
              .state('hr_manager.doctor.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/hr_manager/doctor/edit.html',

              })
              
              
              //appointment
              .state('hr_manager.secretary', {
                  abstract: true,
                  url: '/secretary',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/hr_manager/secretary/controller.js']);
                      }]
                    }
              })
              .state('hr_manager.secretary.list', {
                  url: '/list',
                  templateUrl: '../../apps/hr_manager/secretary/list.html',
              })
              .state('hr_manager.secretary.add', {
                  url: '/add',
                  templateUrl: '../../apps/hr_manager/secretary/add.html',
              })
              .state('hr_manager.secretary.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/hr_manager/secretary/edit.html',
              })
              .state('hr_manager.secretaryt.view', {
                  url: '/view/:id',
                  templateUrl: '../../apps/hr_manager/secretary/view.html',
              })
                    //patient
              .state('hr_manager.staff', {
                  abstract: true,
                  url: '/staff',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/hr_manager/staff/controller.js']);
                      }]
                    }
              })
              .state('hr_manager.staff.list', {
                  url: '/list',
                  templateUrl: '../../apps/hr_manager/staff/list.html',
              })
              .state('hr_manager.staff.add', {
                  url: '/add',
                  templateUrl: '../../apps/hr_manager/staff/add.html',
              })
              .state('hr_manager.staff.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/hr_manager/staff/edit.html',

              })
      }
    ]
  );
